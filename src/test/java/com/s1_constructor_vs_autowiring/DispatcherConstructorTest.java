package com.s1_constructor_vs_autowiring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DispatcherConstructorTest {

    private MessageDispatcher dispatcher;

    @BeforeEach
    public void prepareDispatcher() {
        dispatcher = new MessageDispatcher();
        dispatcher.setMessageBean(new MessageBean());
    }

    @Test
    public void testIt() {
        dispatcher.dispatchMessage();
    }
}