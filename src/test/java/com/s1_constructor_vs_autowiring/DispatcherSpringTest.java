package com.s1_constructor_vs_autowiring;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class DispatcherSpringTest extends SpringTest {

    //add annotations.
    @Autowired
    private MessageDispatcher dispatcher;

    @Test
    public void testIt() {
        dispatcher.dispatchMessage();
    }
}