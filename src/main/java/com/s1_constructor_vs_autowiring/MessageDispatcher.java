package com.s1_constructor_vs_autowiring;

public class MessageDispatcher {

    private MessageBean messageBean;

    public void dispatchMessage() {
        System.out.println(messageBean.getMessage());
    }

    void setMessageBean(MessageBean messageBean) {
        this.messageBean = messageBean;
    }
}
