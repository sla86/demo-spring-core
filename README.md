# spring-demo

CI/IoC theory
Context explanation
	+ (master) Constructor VS Autowiring example
	- Context configuration
	- Simple explanation of Bean Definition
	- DAO example
Bean scope
	- Singleton vs Prototype theory
Autowiring
	- constructor
	- field
	- setter
Bean definition.
	- XML
	- JAVA
	- Annotation
Bean as collections
	- List
	- Map
	- ...
Bean Lifecycle
	- Bean's methods
	- BeanPostProcessor interface
	- InitializingBean interface
	- DisposableBean interface
Bean Inheritance
Inner bean
Properties
Testing with SpringExtension